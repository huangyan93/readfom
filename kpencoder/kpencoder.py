import numpy as np
import json
import math
#---construct a data processing class for encoding, quantization, and decoding
class myKpProcessor():
    def __init__(self, filein, NumFrame, NumKp, range):
        self.NumFrame = NumFrame
        self.NumKp = NumKp
        self.filein = filein

        self.dataOrg = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.float16)
        self.dataOrgUni = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.int16)
        self.dataPred = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.int16)
        self.dataResi = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.int16)
        self.dataBin = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.int16)
        self.dataSideInfo = np.zeros((self.NumFrame, 1), dtype = np.float16)
        self.dataRec = np.zeros((self.NumFrame, self.NumKp, 6), dtype = np.float16)

        self.NumBin = 0
        self.range = range

    def LoadAllData(self):
        with open(self.filein) as f:
            keyPoint = json.load(f)
        location = []
        for FrameIdx in range(len(keyPoint)):
            location = np.float16(np.array(keyPoint[str(FrameIdx)]['value']).squeeze().reshape(-1).tolist())
            #print(location)
            for KpIdx in range (0, self.NumKp):
                self.dataOrg[FrameIdx, KpIdx, 0] = location[KpIdx*2]
                self.dataOrg[FrameIdx, KpIdx, 1] = location[KpIdx*2 + 1]
        jacobian = []
        for FrameIdx in range(len(keyPoint)):
            jacobian = np.float16(np.array(keyPoint[str(FrameIdx)]['jacobian']).squeeze().reshape(-1).tolist())
            #print(jacobian)
            for KpIdx in range (0, self.NumKp):
                self.dataOrg[FrameIdx, KpIdx, 2] = jacobian[KpIdx*4]
                self.dataOrg[FrameIdx, KpIdx, 3] = jacobian[KpIdx*4 + 1]
                self.dataOrg[FrameIdx, KpIdx, 4] = jacobian[KpIdx*4 + 2]
                self.dataOrg[FrameIdx, KpIdx, 5] = jacobian[KpIdx*4 + 3]
            self.dataSideInfo[FrameIdx] = max(abs(self.dataOrg[FrameIdx, :, 2:6].reshape(-1)))
        #print(self.dataOrg)
    
    def Analysis(self):
        print("minLoc:", min(self.dataOrg[:,:,0:2].reshape(-1)), "maxLoc:", max(self.dataOrg[:,:,0:2].reshape(-1)))
        print("minJacob:", min(self.dataOrg[:,:,2:6].reshape(-1)), "maxJacob:", max(self.dataOrg[:,:,2:6].reshape(-1)))
        #print(self.dataSideInfo.reshape(-1))
    
    def Preprocess(self):
        # process Loc
        self.dataOrgUni[:, :, 0: 2] = np.around((self.dataOrg[:, :, 0: 2] + 1) * (self.range / 2))
        #print(tmpBuffer)
        #self.dataOrgUni[:, :, 0: 2] = np.where(tmpBuffer > self.range - 1, self.range - 1, tmpBuffer)
        # process Jacob
        for FrameIdx in np.arange(self.NumFrame):
            self.dataOrgUni[FrameIdx, :, 2: 6] = np.around((self.dataOrg[FrameIdx, :, 2: 6] / self.dataSideInfo[FrameIdx] + 1) * (self.range / 2))
            #print(tmpBuffer)
            #self.dataOrgUni[FrameIdx, :, 2: 6] = np.where(tmpBuffer > self.range - 1, self.range - 1, tmpBuffer)
        
        np.clip(self.dataOrgUni, 0, self.range - 1)
        #print(self.dataOrg)
        #print(self.dataOrgUni)

    def EncodeAndPrintInfo(self):
        def dec2bin(x, k):
            bits2 = 0
            Bitstream2 = ""
            while (bits2 < k):
                tmpbin = x % 2
                Bitstream2 = str(tmpbin) + Bitstream2
                bits2 = bits2 + 1
                x = x // 2
            return Bitstream2, bits2
        def ZeroExpGolomb(x):
            x = x + 1
            bits1 = 0
            Bitstream1 = ""
            while (x != 0):
                tmpbin = x % 2
                Bitstream1 = str(tmpbin) + Bitstream1
                bits1 = bits1 + 1
                x = x // 2
            for i in range(bits1 - 1):
                Bitstream1 = "0" + Bitstream1
            return Bitstream1, 2*bits1-1
        def expGolombEnc(x, k):
            y = 0
            if x <= 0:
                y = (-2) * x
            else:
                y = 2 * x - 1
            # y = A + B
            A = y // (2**k)
            B = y % (2**k)
            Bitstream1, bits1 = ZeroExpGolomb(A)
            Bitstream2, bits2 = dec2bin(B, k)
            #print(Bitstream1, bits1, Bitstream2, bits2)
            return bits1 + bits2

        # start k-th exp Golomb encoding
        ks = [0,1,2,3,4]
        #ks = [2]
        for k in ks:
            # to record the bit consumptionn of input and output
            sumBitsInput = 0
            sumBitsOutput = 0
            sumBitsInputloc = 0
            sumBitsInputmv = 0
            sumBitsOutputloc = 0
            sumBitsOutputmv = 0

            # loc compress ratio calc
            for data in self.dataResi[:,:,0:2].reshape(-1):
                bits = expGolombEnc(data, k)
                sumBitsInputloc = sumBitsInputloc + int(math.log(self.range, 2))
                sumBitsOutputloc = sumBitsOutputloc + bits
            # mv compress ratio calc
            for data in self.dataResi[:,:,2:6].reshape(-1):
                bits = expGolombEnc(data, k)
                sumBitsInputmv = sumBitsInputmv + int(math.log(self.range, 2))
                sumBitsOutputmv = sumBitsOutputmv + bits
            # overall calc
            for data in self.dataResi.reshape(-1):
                #print(data)
                bits = expGolombEnc(data, k)
                sumBitsInput = sumBitsInput + int(math.log(self.range, 2))
                sumBitsOutput = sumBitsOutput + bits
                #print(sumBitsInput, sumBitsOutput)
            # decode and see the results
            print(k, "th Exponential Golomb Encoding: ","Overall compressratio: ", sumBitsOutput/sumBitsInput)
            print("loc compression ratio: ", sumBitsOutputloc/sumBitsInputloc)
            print("mv compression ratio: ", sumBitsOutputmv/sumBitsInputmv)
            

    def Compress(self):
        def IntraPrediction(FrameIdx):
            for KpIdx in np.arange(self.NumKp):
                if KpIdx == 0:
                    self.dataPred[FrameIdx, KpIdx, :] = self.range / 2
                else:
                    # predicted through the previous key point
                    self.dataPred[FrameIdx, KpIdx, :] = self.dataOrgUni[FrameIdx, KpIdx - 1, :]
        def InterPrediction(FrameIdx):
            # predicted through the previous frame
            self.dataPred[FrameIdx, :, :] = self.dataOrgUni[FrameIdx - 1, :, :]
        for FrameIdx in np.arange(self.NumFrame):
            if FrameIdx == 0:
                IntraPrediction(FrameIdx)
            else:
                InterPrediction(FrameIdx)
        self.dataResi = self.dataOrgUni - self.dataPred
        #print(self.dataResi)

    def Decode(self):
        #tmpBuffer = self.dataOrgUni[:, :, 0: 2] / (self.range / 2) - 1
        self.dataRec[:, :, 0: 2] = self.dataOrgUni[:, :, 0: 2] / (self.range / 2) - 1
        for FrameIdx in np.arange(self.NumFrame):
            self.dataRec[FrameIdx, :, 2: 6] = (self.dataOrgUni[FrameIdx, :, 2: 6] / (self.range / 2) - 1) * self.dataSideInfo[FrameIdx]
        #print(self.dataOrgUni)
        #print(self.dataRec)
        print("SAD of loc: ", np.mean(abs(self.dataRec[:,:, 0:2] - self.dataOrg[:,:, 0:2])))
        print("SAD of mv: ", np.mean(abs(self.dataRec[:,:, 2:6] - self.dataOrg[:,:, 2:6])))

#---main---
if __name__ == "__main__":
    NumFrame = 100
    NumKp = 10
    filein = "/Users/huangyan/Desktop/kpencoder/kp_detector_output.json"

    enc = myKpProcessor(filein, NumFrame = NumFrame, NumKp = NumKp, range = 1024)
    #--load data from source file--
    enc.LoadAllData()
    #--Analysis min/max data--
    enc.Analysis()
    #--unify into range, e.g., 0-255--
    enc.Preprocess()
    #--Encode with diff QP params--
    enc.Compress()
    #--Print original data volumn and compressed data volumn--
    enc.EncodeAndPrintInfo()
    #--Reconstruct data from the compressed data--
    enc.Decode()

